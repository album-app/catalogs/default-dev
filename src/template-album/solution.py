from io import StringIO

from album.runner.api import setup

env_file = StringIO("""channels:
  - conda-forge
  - defaults
dependencies:
  - python=3.8
  - pip=21.2.4
  - git=2.35.0
  - mamba=0.20.0
  - pip:
    - album==0.3.1
""")


def init_album_solutions():
    from album.api import Album
    from album.runner.api import get_app_path
    from album.runner.album_logging import LogLevel

    album = Album.Builder().base_cache_path(get_app_path()).log_level(LogLevel.DEBUG).build()
    album.load_or_create_collection()

    album.add_catalog("https://gitlab.com/album-app/catalogs/default-dev")

    solution1 = "album:template-imagej2:0.1.0"
    solution2 = "album:template-napari:0.1.0"

    return album, solution1, solution2


def install():
    album, solution1, solution2 = init_album_solutions()

    if not album.is_installed(solution1):
        album.install(solution1)
    if not album.is_installed(solution2):
        album.install(solution2)


def run():
    from album.runner.api import get_data_path

    album, solution1, solution2 = init_album_solutions()

    image_path = str(get_data_path().joinpath("test.tif"))
    album.run(solution1, argv=["", "--output_image_path", image_path])
    album.run(solution2, argv=["", "--input_image_path", image_path])


setup(
    group="album",
    name="template-album",
    version="0.1.0",
    title="Album template",
    description="An Album solution template for running album inside album.",
    authors=["Album team"],
    cite=[{
        "text": "J. P. Albrecht, D. Schmidt, K. Harrington. Album: a framework for scientific data processing with software solutions of heterogeneous tools.",
        "doi": "arXiv:2110.00601",
        "url": "https://album.solutions"
    }],
    tags=["template", "album"],
    license="unlicense",
    documentation=["documentation.md"],
    covers=[{
        "description": "Dummy cover image.",
        "source": "cover.png"
    }],
    album_api_version="0.3.1",
    install=install,
    run=run,
    dependencies={'environment_file': env_file}
)
